import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class Test_TestAssignment {

    private final MatrixParser parser = new MatrixParser();
    private final MatrixCalculator calculator = new MatrixCalculator();

    private static Stream<Arguments> getMatricesWithInvalidNames() {
        return Stream.of(
                Arguments.of("q=[9 8; -10 -4; -3 -9]"),
                Arguments.of("MM=[-6 4; 4 10]"),
                Arguments.of("1=[5 -5; 0 6; 3 -4]")
        );
    }

    private static Stream<Arguments> getMatricesWithInvalidValues() {
        return Stream.of(
                Arguments.of("Q=[9 8-; -10 -4; -3 -9]"),
                Arguments.of("M=[-6f 4; 4 10]")
        );
    }

    private static Stream<Arguments> getValidArraysToExecuteBasicActions() {
        int[][] plusFirstOperand = {{1, 2}, {1, 2}};
        int[][] plusSecondOperand = {{2, 1}, {2, 1}};
        int[][] firstExpected = {{3, 3}, {3, 3}};
        int[][] minusFirstOperand = {{10, 5}, {5, 1}};
        int[][] minusSecondOperand = {{10, 5}, {5, 1}};
        int[][] secondExpected = {{0, 0}, {0, 0}};

        return Stream.of(
                Arguments.of(plusFirstOperand, plusSecondOperand, firstExpected, "+"),
                Arguments.of(minusFirstOperand, minusSecondOperand, secondExpected, "-")
        );
    }

    private static Stream<Arguments> getInvalidArraysToExecuteMultiply() {
        int[][] plusFirstOperand = {{1, 2, 2}, {1, 2, 3}};
        int[][] plusSecondOperand = {{2, 1}, {2, 1}};

        return Stream.of(Arguments.of(plusFirstOperand, plusSecondOperand));
    }

    @ParameterizedTest
    @CsvSource({"Q=[9 8; -10 -4; -3 -9], Q", "M=[-6 4; 4 10], M", "F=[5 -5; 0 6; 3 -4], F"})
    void when_parseMatrix_then_nameShouldBeValid(String inputLine, String expectedName) {
        // when
        Matrix matrix = parser.parseMatrix(inputLine);
        // then
        assertEquals(expectedName, matrix.getName(), "");
    }

    @ParameterizedTest
    @MethodSource("getMatricesWithInvalidNames")
    void when_parseMatrixWithInvalidNames_then_throwException(String inputLine) {
        // when
        var exception =
                assertThrows(IllegalArgumentException.class, () -> parser.parseMatrix(inputLine));
        // then
        assertEquals("Invalid matrix name.", exception.getMessage());
    }

    @ParameterizedTest
    @CsvSource({"Q=[9 8; -10 -4; -3 -9], 3, 2", "M=[-6 4; 4 10], 2, 2", "F=[5 -5; 0 6; 3 -4], 3, 2"})
    void when_parserParseMatrix_then_arrayShouldBeValid(String inputLine,
                                                        int expectedOuterLength,
                                                        int expectedInnerLength) {
        // when
        Matrix matrix = parser.parseMatrix(inputLine);
        int[][] array = matrix.getArray();
        // then
        assertEquals(expectedOuterLength, array.length, "");
        assertEquals(expectedInnerLength, array[0].length, "");
    }

    @ParameterizedTest
    @MethodSource("getMatricesWithInvalidValues")
    void when_parseMatrixWithInvalidValues_then_throwException(String inputLine) {
        // when
        var exception =
                assertThrows(IllegalArgumentException.class, () -> parser.parseMatrix(inputLine));
        // then
        assertEquals("Can't read matrix.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("getValidArraysToExecuteBasicActions")
    void when_executeBaseAction_then_arraysShouldBeEquals(int[][] firstOperand,
                                                          int[][] secondOperand,
                                                          int[][] expectedResult,
                                                          String action) {
        // when
        int[][] actualResult = calculator.executeBaseAction(firstOperand, secondOperand, action);
        // then
        assertTrue(Arrays.deepEquals(expectedResult, actualResult), "");
    }

    @ParameterizedTest
    @MethodSource("getInvalidArraysToExecuteMultiply")
    void when_executeMultiply_then_shouldThrowException(int[][] firstOperand, int[][] secondOperand) {
        // when
        var exception =
                assertThrows(IllegalArgumentException.class, () -> calculator.executeMultiply(firstOperand, secondOperand));
        // then
        assertEquals("Can't perform multiplication", exception.getMessage());
    }

}