import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Implementation of parsing incoming line (String) to Matrix.
 *
 * @author Dzmitry Turko
 * @see Matrix
 */
public class MatrixParser {

    private static final String EXTRA_RESULT_SYMBOLS = "[\\[\\],]";
    private static final String EXTRA_INPUT_SYMBOLS = "[\\[\\]]";
    private static final String MATRIX_NAME_PATTERN = "[A-Z]";
    private static final String INNER_ARRAY_SEPARATOR = "; ";
    private static final String RESULT_PREFIX = "[";
    private static final String RESULT_POSTFIX = "]";
    private static final String EMPTY_STRING = "";
    private static final String SEMICOLON = ";";
    private static final String SPACE = " ";
    private static final String EVEN = "=";

    private static final int MATRIX_NAME_INDEX = 0;
    private static final int MATRIX_VALUE_INDEX = 1;

    /**
     * Parses string to Matrix and validates it's parsed name.
     *
     * @param inputLine a line with '=' separated matrix name and array.
     * @return parsed Matrix.
     * @throws IllegalArgumentException if the matrix name is not matches the pattern or
     *                                  if the given string does not have the appropriate number format.
     */
    public Matrix parseMatrix(String inputLine) {
        String[] input = inputLine.split(EVEN);
        validateMatrixName(input[MATRIX_NAME_INDEX]);

        String[] rowsInput = input[MATRIX_VALUE_INDEX].replaceAll(EXTRA_INPUT_SYMBOLS, EMPTY_STRING).split(SEMICOLON);

        int[][] rows = new int[rowsInput.length][];
        for (int i = 0; i < rows.length; i++) {
            String[] items = rowsInput[i].trim().split(SPACE);

            int[] row = new int[items.length];
            for (int j = 0; j < row.length; j++) {
                try {
                    row[j] = Integer.parseInt(items[j]);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Can't read matrix.");
                }
            }

            rows[i] = row;
        }

        return new Matrix(input[MATRIX_NAME_INDEX], rows);
    }

    /**
     * Parses given array to human readable form.
     * Deletes extra symbols, separate nested arrays by semicolon and
     * surrounds the result with brackets.
     *
     * @param resultArray an array to parse.
     * @return human readable result of array.
     */
    public String parseArrayToReadableString(int[][] resultArray) {
        return Arrays.stream(resultArray)
                .map(Arrays::toString)
                .map(s -> s.replaceAll(EXTRA_RESULT_SYMBOLS, EMPTY_STRING))
                .collect(Collectors.joining(INNER_ARRAY_SEPARATOR, RESULT_PREFIX, RESULT_POSTFIX));
    }

    private void validateMatrixName(String matrixName) {
        if (!matrixName.matches(MATRIX_NAME_PATTERN)) {
            throw new IllegalArgumentException("Invalid matrix name.");
        }
    }
}