/**
 * This class is responsible for applying three types of actions
 * between operands (addition, subtraction and multiplication).
 *
 * @author Dzmitry Turko
 */
public class MatrixCalculator {

    private static final String PLUS = "+";

    /**
     * Executes multiplying two operands.
     * Before applying operation validates possibility of multiplying.
     *
     * @param firstOperand  left operand to multiply.
     * @param secondOperand right operand to multiply.
     * @return int[][] as multiply result.
     * @throws IllegalArgumentException if multiplying isn't possible.
     */
    public int[][] executeMultiply(int[][] firstOperand, int[][] secondOperand) {
        validateMultiply(firstOperand, secondOperand);

        int[][] result = new int[firstOperand.length][secondOperand[0].length];
        for (int i = 0; i < firstOperand.length; ++i)
            for (int j = 0; j < secondOperand[0].length; ++j) {
                result[i][j] = 0;
                for (int k = 0; k < firstOperand[0].length; ++k)
                    result[i][j] += firstOperand[i][k] * secondOperand[k][j];
            }

        return result;
    }

    /**
     * Method executes operation between two operands.
     * Executes plus or minus action ONLY.
     *
     * @param firstOperand  left operand
     * @param secondOperand right operand
     * @param action        action to execute
     * @return int[][] as a result of operation
     */
    public int[][] executeBaseAction(int[][] firstOperand, int[][] secondOperand, String action) {
        int length = firstOperand.length;
        int innerLength = firstOperand[0].length;

        int[][] result = new int[length][innerLength];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < innerLength; j++) {
                result[i][j] = PLUS.equals(action) ? firstOperand[i][j] + secondOperand[i][j] :
                        firstOperand[i][j] - secondOperand[i][j];
            }
        }

        return result;
    }

    private void validateMultiply(int[][] firstOperand, int[][] secondOperand) {
        if (firstOperand[0].length != secondOperand.length) {
            throw new IllegalArgumentException("Can't perform multiplication");
        }
    }
}
