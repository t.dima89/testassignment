import java.text.MessageFormat;
import java.util.Scanner;

/**
 * Class with entry point. Provides required dependencies and creation of MatrixService.
 * Wrap all checked exceptions and redirect message to the basic output.
 *
 * @author Dzmitry Turko
 * @see MatrixService
 */
public class TestAssignment {

    public static void main(String[] args) {
        MatrixService matrixService = new MatrixService(new MatrixParser(), new MatrixCalculator());

        try {
            Scanner scanner = new Scanner(System.in);
            matrixService.proceedInput(scanner);
            matrixService.proceedCalculations();
            matrixService.proceedOutput();

        } catch (Exception e) {
            System.err.println(MessageFormat.format("Exception caught: {0}. {1}",
                    e.getClass().getSimpleName(), e.getMessage()));
        }
    }
}