import java.util.*;

/**
 * A service to coordinate Matrix calculations.
 * Serves to store parsed matrices, operands and actions.
 * <p>
 * This implementation execute reading input information,
 * execute calculations with matrices (addition, subtraction and multiplication),
 * and print the result of calculations to basic output.
 *
 * @author Dzmitry Turko
 * @see MatrixParser
 * @see MatrixCalculator
 */
public class MatrixService {

    private static final String HIGH_PRIORITY_ACTION = "*";
    private static final String MATRIX_INDICATOR = "=";

    private final MatrixParser matrixParser;
    private final MatrixCalculator matrixCalculator;

    private final Map<String, int[][]> matrices = new HashMap<>();
    private final Deque<String> operands = new LinkedList<>();
    private final Deque<String> basePriorityActions = new LinkedList<>();

    private String formula;

    public MatrixService(MatrixParser matrixParser, MatrixCalculator matrixCalculator) {
        this.matrixParser = matrixParser;
        this.matrixCalculator = matrixCalculator;
    }

    /**
     * Attempts to parse scanner's input line by line
     * to find any matches about matrices or formula.
     *
     * @param scanner a scanner to read input.
     * @throws IllegalArgumentException if the parsed matrices or formula is invalid.
     */
    public void proceedInput(Scanner scanner) {
        while (scanner.hasNext()) {
            String inputLine = scanner.nextLine();
            if (inputLine.contains(MATRIX_INDICATOR)) {
                Matrix matrix = matrixParser.parseMatrix(inputLine);
                matrices.put(matrix.getName(), matrix.getArray());

            } else if (!inputLine.isBlank()) {
                formula = inputLine;
            }
        }

        validateParsedData(matrices, formula);
    }

    /**
     * Go through saved formula and prepare Deques with operands and less priority actions.
     * If there is any high priority operation in preparation, executes them immediately
     * and saves the result of operation in common way to operands and matrices.
     * <p>
     * After preparation, execute calculation one by one from Deques.
     */
    public void proceedCalculations() {
        prepareOperandsAndAction();
        baseActionsIteration();
    }

    /**
     * Print the result of calculations to basic output.
     */
    public void proceedOutput() {
        int[][] resultArray = matrices.get(operands.getLast());
        System.out.println(matrixParser.parseArrayToReadableString(resultArray));
    }

    private void prepareOperandsAndAction() {
        for (int i = 0; i < formula.length(); i++) {
            String item = String.valueOf(formula.charAt(i));

            if (matrices.containsKey(item)) {
                operands.addLast(item);

            } else if (HIGH_PRIORITY_ACTION.equals(item)) {
                proceedFirstPriorityAction(i++);

            } else {
                basePriorityActions.addLast(item);
            }
        }
    }

    private void proceedFirstPriorityAction(int i) {
        String firstOperandName = operands.pollLast();
        String secondOperandName = String.valueOf(formula.charAt(i + 1));

        int[][] firstOperand = matrices.get(firstOperandName);
        int[][] secondOperand = matrices.get(secondOperandName);

        String newMatrixName = firstOperandName + HIGH_PRIORITY_ACTION + secondOperandName;
        matrices.put(newMatrixName, matrixCalculator.executeMultiply(firstOperand, secondOperand));
        operands.addLast(newMatrixName);
    }

    private void baseActionsIteration() {
        for (String action : basePriorityActions) {
            String firstKey = operands.pollFirst();
            String secondKey = operands.pollFirst();

            int[][] firstOperand = matrices.get(firstKey);
            int[][] secondOperand = matrices.get(secondKey);

            String newMatrixName = firstKey + action + secondKey;
            matrices.put(newMatrixName, matrixCalculator.executeBaseAction(firstOperand, secondOperand, action));
            operands.addFirst(newMatrixName);
        }
    }

    private void validateParsedData(Map<String, int[][]> matrices, String formula) {
        if (formula == null || formula.isBlank()) {
            throw new IllegalArgumentException("Please, provide any formula.");
        }

        if (matrices.isEmpty()) {
            throw new IllegalArgumentException("Please, provide any matrices.");
        }
    }
}




