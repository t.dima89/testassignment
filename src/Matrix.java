/**
 * Class serves to store matrix data and treading like one object.
 */
public class Matrix {

    private String name;
    private int[][] array;

    public Matrix(String name, int[][] array) {
        this.name = name;
        this.array = array;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[][] getArray() {
        return array;
    }

    public void setArray(int[][] array) {
        this.array = array;
    }
}
